#' Run the Shiny Application
#'
#' @export
#' @importFrom shiny shinyApp
#' @importFrom golem with_golem_options
run_app <- function(...) {
  shiny::runApp(system.file("app",package = "covidDashboard"),port=3838 ,host = "0.0.0.0")
}
