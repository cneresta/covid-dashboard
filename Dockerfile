FROM rocker/tidyverse:3.6.3
#ENV HTTP_PROXY http://web-proxy.bordeaux.inria.fr:3128
#ENV HTTPS_PROXY http://web-proxy.bordeaux.inria.fr:3128
RUN echo http proxy: ${http_proxy} ${HTTP_PROXY}
RUN echo https proxy: ${https_proxy} ${HTTPS_PROXY}
#ARG http_proxy
#ARG https_proxy
#RUN env | grep -i http
#RUN apt-get update && apt-get upgrade
#RUN wget https://mirror.ibcp.fr/pub/CRAN/
RUN R -e 'install.packages("remotes", dependencies=TRUE, repos="https://mirror.ibcp.fr/pub/CRAN/")' 
RUN R -e 'remotes::install_github("r-lib/remotes", ref = "9b5dc29")' 
RUN R -e 'install.packages(c("shiny","golem","attempt","dplyr","DT","glue","htmltools","thinkr","ggplot2","scales","leaflet","raster","pkgload","EpiEstim","forestplot","shinydashboard","processx","thinkr","shinydashboard","dplyr","tidyverse","beeswarm","plotly","rlist","waiter","testhat","knitr","rmarkdown","fs","rvg","ggiraph","waiter","latex2exp"), repos="https://mirror.ibcp.fr/pub/CRAN/", dependencies=TRUE)' 
COPY covidDashboard_0.0.0.9000.tar.gz /app.tar.gz 
RUN R -e 'remotes::install_local("/app.tar.gz")' 
EXPOSE 3838 
CMD ["R", "-e options('shiny.port'=3838,shiny.host='0.0.0.0'); covidDashboard::run_app()"]
